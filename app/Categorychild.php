<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorychild extends Model
{
    protected $guarded = [];
    protected $fillalbe = [
        'id','name','category_id'
    ];
    public $timestamps = false;
    public function products() {
        return $this->hasMany('App\Product','categorychildren_id');
    }
    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }
}
