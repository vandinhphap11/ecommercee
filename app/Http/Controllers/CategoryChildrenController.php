<?php

namespace App\Http\Controllers;

use App\Category;
use App\Categorychild;
use Illuminate\Http\Request;

class CategoryChildrenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $catechildren = Categorychild::all();
        return view ('admin.categorychild.main',compact('catechildren'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('admin.categorychild.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $categorychild = new Categorychild($request->all());
        $categorychild->save();
        return redirect('/categorychild');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all();
        $categorychild = Categorychild::findOrFail($id);
        return view('admin.categorychild.edit', compact('categorychild','category'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|string',
        ]);

        $categorychild = Categorychild::find($id);
        $categorychild->update($request->all());

        return redirect('/categorychild');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Categorychild::find($id)->delete();
        return redirect()->back();
        // dd($category);
    }
}
