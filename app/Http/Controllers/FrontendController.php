<?php

namespace App\Http\Controllers;

use App\Category;
use App\Categorychild;
use App\Customer;
use App\Manufacturers;
use App\Order;
use App\OrderDetail;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{   
    protected $product;
    protected $orderDetail;
    protected $customer;
    protected $order;
    protected $categorychild;

    public function __construct(Category $category,Product $product,OrderDetail $orderDetail,Customer $customer, Order $order, Categorychild $categorychild)
    {
        $this->category  = $category;
        $this->product = $product;
        $this->orderDetail = $orderDetail;
        $this->customer = $customer;
        $this->order = $order;
        $this->categorychild = $categorychild;
        
    }
    public function listData()
    {
        $countProduct = DB::table('order_detail')->selectRaw('products_id, count(products_id) as topOrder')->groupBy('products_id')->orderBy('topOrder','DESC')->limit(12)->get();
        foreach($countProduct as $count){
            $productIdCountArr[] = $count->products_id; 
        }
        $topProduct = $this->product::whereIn('id',$productIdCountArr)->get();
        $manufacturers = Manufacturers::all();
        $categorychild = $this->categorychild::limit(3)->get();
        foreach($categorychild as $productMenu){
            $product = $this->product::where('categorychildren_id',$productMenu->id)->limit(5)->get();
            $productMenu->product = $product;
        }
        $categories = Category::limit(3)->get();
        $customers = $this->customer::all();
        $productRandom = $this->product::all()->random(10);
        $newProducts = Product::orderBy('id', 'DESC')->limit(10)->get();
        return view('frontend.page.home', compact([
            'newProducts', 'productRandom', 'categories',
            'manufacturers', 'customers', 'categorychild', 'topProduct'
        ]));
    }
    public function productTab($id)
    {
        if ($id == '*') {
            $products = $this->product::all()->random(12);
        } else {
            $products = $this->product::where('category_id', $id)->get();
        }
        // dd($products);
        foreach ($products as $product) {
            $product->image = str_replace('public', '', 'storage/' . $product->image);
            $product->price = number_format((float)$product->price, 0, '.', ',');
        }
        return response()->json($products, 200);
    }
    public function search()
    {
        return view('frontend.page.search');
    }
    public function searchByName(Request $request)
    {
        $data = [];
        $research = $this->product::where('name', 'like', '%' . $request->search . '%')->get();
        // dd(count($research));
        $data = [
            'search' => $research,
            'message' => '"' . count($research) . '" tìm thấy kết quả cho từ khoá"' . $request->search . '"'
        ];
        return view('frontend.page.search', compact('data'));
    }
    public function productDetail($id)
    {
        $product = $this->product::find($id);
        return view('frontend.page.productdetail', compact('product'));
    }
    public function addToCart(Request $request)
    {
        try {
            $total = 0;
            $product = $this->product::findOrFail($request->dataID);

            if (\Cart::count() == 0) {
                \Cart::add([
                    'id' => $request->dataID,
                    'name' => $product->name,
                    'qty' => $request->qty ? $request->qty : 1,
                    'price' => $product->price,
                    'weight' => 0,
                    'options' => [str_replace('public', '', 'storage' . $product->image)]
                ]);
                foreach (\Cart::content() as $cartItem) {
                    $total += $cartItem->qty * $cartItem->price;
                }
                return $this->jsonDataResult([
                    'cart' => \Cart::content(),
                        'total' => number_format($total,0,'.',','),
                        'count' => \Cart::count()
                ], 200);
            }

            $productByID = \Cart::content()->where('id', $request->dataID);
            
            if (count($productByID) > 0) {
                $total = 0;
                foreach ($productByID as $productByIdItem) {
                    $rowId = $productByIdItem->rowId;
                }
                if ($request->qty) {
                    $productByID[$rowId]->qty += $request->qty;
                } else {
                    $productByID[$rowId]->qty++;
                }
                foreach (\Cart::content() as $cartItem) {
                    $total += $cartItem->qty * $cartItem->price;
                }
                return $this->jsonDataResult([
                    'cart' => \Cart::content(),
                        'total' => number_format($total,0,'.',','),
                        'count' => \Cart::count()
                ], 200);
            }
            \Cart::add([
                'id' => $request->dataID,
                'name' => $product->name,
                'qty' => $request->qty ? $request->qty : 1,
                'price' => $product->price,
                'weight' => 0,
                'options' => [str_replace('public', '', 'storage' . $product->image)]
            ]);
            $total = 0;
            foreach (\Cart::content() as $cartItem) {
                $total += $cartItem->qty * $cartItem->price;
            }
            return $this->jsonDataResult([
                    'cart' => \Cart::content(),
                        'total' => number_format($total,0,'.',','),
                        'count' => \Cart::count()
            ], 200);
        } catch (\Exception $e) {
            return $this->jsonMsgResult($e->getMessage(),false,500);
        }
    }
    public function updateCart(Request $request)
    {
        $request->validate([
            'qty' => 'required|integer|min:0',
        ]);
        $total = 0;
        if ($request->dataID) {
            \Cart::content()[$request->dataID]->qty = $request->qty;
        }
        $subPrice = number_format(\Cart::content()[$request->dataID]->qty * \Cart::content()[$request->dataID]->price,0,'.',',').'đ';
        foreach (\Cart::content() as $cartItem) {
            $total += $cartItem->qty * $cartItem->price;
        }
        return $this->jsonDataResult([
            'subPrice' => $subPrice,
            'cart' => \Cart::content()[$request->dataID],
            'total' => number_format($total, 0, '.', ',') . 'đ'
        ],200);
    }
    public function Cart()
    {
        return view('frontend.page.cart');
    }
    public function removeCart(Request $request)
    {
        $total = 0;
        \Cart::remove($request->dataID);
        foreach(\Cart::content() as $cartItem){
            $total = $cartItem->qty * $cartItem->price;
        }
        return $this->jsonDataResult([
            'total' => $total,
            'count' => \Cart::count()
        ],200);
    }
    public function Checkout()
    {
        $total = 0;
        foreach (\Cart::content() as $cartItem) {
            $total += $cartItem->qty * $cartItem->price;
        }
        return view('frontend.page.checkout', compact('total'));
    }
    public function orderDetail(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'address' => 'required|string|max:255',
            'phone' => 'required',
            'note' => 'required',
        ]);
        $newCustomer = new $this->customer($request->all());
        $newCustomer->save();
        $newOrder = new $this->order();
        $newOrder->customers_id = $newCustomer->id;
        $newOrder->ordernumber = 'abc';
        $newOrder->save();
        $cartCollection = \Cart::content();
        foreach ($cartCollection as $product) {
            $orderDetail = new $this->OrderDetail();
            $orderDetail->products_id = (int) $product->id;
            $orderDetail->order_id = $newOrder->id;
            $orderDetail->ordernumber = 1;
            $orderDetail->price = $product->price;
            $orderDetail->quantity = $product->qty;
            $orderDetail->total = $product->qty * $product->price;
            $orderDetail->save();
        }
        return redirect()->route('home.success', ['order_id' => $newOrder->id, 'customer_id' => $newCustomer->id]);
    }
    public function Success($order_id, $customer_id)
    {
        $orderDetail = $this->OrderDetail::where('order_id', $order_id)->get();
        $totalPrice = 0;
        foreach ($orderDetail as $orderDetailByProduct) {
            $products = $this->product::where('id', $orderDetailByProduct->products_id)->get();
            $orderDetailByProduct->product = $products;
            $totalPrice += $orderDetailByProduct->quantity * $orderDetailByProduct->price;
        }
        $order = Order::where('customers_id', $customer_id)->get();
        foreach ($order as $orderItem) {
            $customerID = $orderItem->customers_id;
        }
        \Cart::destroy();

        $customer = $this->customer::find($customerID);
        return view('frontend.page.successCheckout', compact('totalPrice', 'orderDetail', 'customer'));
    }
    public function showProduct(Request $request, $id)
    {

        $products = $this->product::where('category_id', $id)->get();
        $categorychild = Categorychild::find($id);
        return view('frontend.page.products', compact(['products', 'categorychild']));
    }
    public function showBrand($id)
    {
        $products = $this->product::where('manufacturers_id', $id)->get();
        $brands = Manufacturers::find($id);
        return view('frontend.page.brands', compact(['brands', 'products']));
    }
    private function jsonMsgResult($errors, $success, $statusCode)
    {
        $result = [
            'errors' => $errors,
            'success' => $success,
            'statusCode' => $statusCode,
        ];

        return response()->json($result, $result['statusCode']);
    }

    /**
     * Return data json format
     */
    private function jsonDataResult($data, $statusCode)
    {
        $result = [
            'data' => $data,
            'statusCode' => $statusCode,
        ];
        return response()->json($result, $result['statusCode']);

    }
}
