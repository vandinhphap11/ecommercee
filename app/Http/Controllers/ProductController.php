<?php

namespace App\Http\Controllers;

use App\Category;
use App\Categorychild;
use App\Customer;
use App\Manufacturers;
use App\Product;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view ('admin.products.main',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manufacturers = Manufacturers::all();
        $categorychild = Categorychild::all();
        $category = Category::all();
        return view ('admin.products.create',compact('manufacturers','categorychild','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'image' => 'required',
            'price' => 'required',
            'checkbox' => 'required',
        ]);
        $path = null;
        $data = $request->all();
        $data['checkbox'] = $request->input('checkbox');
        if ($request->file('image')) {
            $path = $request->file('image')->store('public/products');
            $data['image'] = $path;
        }
        Product::create($data);
        return  redirect('/product')->with('message', 'Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customers = Customer::all();
        $product = Product::find($id);
        // dd($product);
        return view ('admin.customers.main',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $manufacturers = Manufacturers::all();
        $product = Product::find($id);
        return view ('admin.products.edit',compact('product','manufacturers','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required',
            'image' => 'nullable',
        ]);

        $products = Product::find($id);

        $path = null;
        $data = $request->all();

        if ($request->file('image')) {
            $path = $request->file('image')->store('public/products');
            $data['image'] = $path;
        }
        $products->update($data);
        return redirect('/product');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect()->back();
    }
}
