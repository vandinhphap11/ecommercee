<?php

namespace App\Http\Controllers;
use App\Order;
use App\Customer;
use App\OrderDetail;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return view ('admin.customers.main',compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        $orders = Order::where('customers_id',$id)->get();
        foreach($orders as $order){
            $orderID[] = $order->id;
        };
        $orderDetails = OrderDetail::whereIn('order_id',$orderID)->get();
        foreach($orderDetails as  $orderdetail){
        $products = Product::where('id',$orderdetail->products_id)->get();
            foreach($products as $product)
            {
                $orderdetail->productName = $product->name;
                $orderdetail->productImage = $product->image;
            }
        
        }
        return view ('admin.customers.show',compact(['orders','customer','orderDetails','products']));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view ('admin.customers.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|string',
        ]);
        $customer = Customer::find($id);
        $customer->update($request->all());

        return redirect('/customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::find($id)->delete();
        return redirect()->back();
    }
}
