<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturers extends Model
{
    protected $fillable = [
        'id','name'
    ];
    public $timestamps = false;
    public function products() {
        return $this->hasMany('App\Product','manufacturer_id');
    }
}
