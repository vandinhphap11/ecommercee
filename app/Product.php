<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id','name','description','price','image','categorychildren_id','manufacturers_id','checkbox','category_id'
    ];
    public function categorychild()
    {
        return $this->belongsTo('App\Categorychild','categorychildren_id');
    }
    public function category()
    {
        return $this->belongsTo('App\Category','category_id');
    }
    public function manufactures() {
        return $this->belongsTo('App\Manufacturers','manufacturers_id');
    }

}
