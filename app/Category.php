<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    protected $fillable = [
        'id','name'
    ];
    public function categorychild() {
        return $this->hasMany('App\Categorychild','category_id');
    }
    public function products() {
        return $this->hasMany('App\Product','category_id');
    }
}
