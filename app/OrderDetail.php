<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{

    protected $table = 'order_detail';
    protected $fillable = [
        'id','products_id','order_id','ordernumber','price','quantity','total'
    ];
    public $timestamps = false;
    public function products() {
        return $this->belongsTo('App\Product','products_id');
    }
}

