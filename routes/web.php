<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//HomePage
Route::name('home.')->group(function(){
    Route::get('/','FrontEndController@listData');
    Route::get('navbar','FrontEndController@navBar');
    Route::get('/detail/{id}','FrontEndController@productDetail')->name('detail');
    Route::post('/addToCart','FrontendController@addToCart')->name('addtocart');
    Route::get('/cart','FrontendController@Cart')->name('cart');
    Route::get('/checkout','FrontendController@Checkout')->name('checkout');
    // Route::get('destroy/{id}', 'FrontendController@removeCart')->name('remove');
    Route::post('order_detail', 'FrontendController@orderDetail')->name('order_detail');
    Route::get('success/{order_id}/{customer_id}', 'FrontendController@Success')->name('success');
    Route::get('products/{id}', 'FrontendController@showProduct')->name('products');
    Route::get('brands/{id}', 'FrontendController@showBrand')->name('brands');
    Route::get('/search','FrontendController@search')->name('search');
    Route::post('/searchPost','FrontendController@searchByName')->name('searchpost');
    Route::get('/product/tab/{category_id}','FrontendController@productTab')->name('tab');
    Route::post('/updateCart','FrontendController@updateCart')->name('updateCart');
    Route::post('/removeCart','FrontendController@removeCart')->name('removeCart');

});

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');


// URLadmin
Route::get('admin',function (){
    return view('admin.app');
});
Route::prefix('product')->group( function () {
    Route::name('product.')->group( function () {
        Route::get('/', 'ProductController@index')->name('index');
        Route::get('/edit/{id}', 'ProductController@edit')->name('edit');
        Route::get('/show/{id}', 'ProductController@show')->name('show');
        Route::put('/update/{id}', 'ProductController@update')->name('update');
        Route::get('/delete/{id}', 'ProductController@destroy')->name('destroy');
        Route::get('/create', 'ProductController@create')->name('create');
        Route::post('/create', 'ProductController@store')->name('store');

    });
});

Route::prefix('category')->group( function () {
    Route::name('category.')->group( function () {
        Route::get('/', 'CategoryController@index')->name('index');
        Route::get('/edit/{id}', 'CategoryController@edit')->name('edit');
        Route::put('/update/{id}', 'CategoryController@update')->name('update');
        Route::get('/delete/{id}', 'CategoryController@destroy')->name('destroy');
        Route::post('/create', 'CategoryController@store')->name('store');
        Route::get('/create', 'CategoryController@create')->name('create');
    });
});
Route::prefix('manufacturers')->group( function () {
    Route::name('manufacturers.')->group( function () {
        Route::get('/', 'ManufacturersController@index')->name('index');
        Route::get('/edit/{id}', 'ManufacturersController@edit')->name('edit');
       
        Route::put('/update/{id}', 'ManufacturersController@update')->name('update');
        Route::get('/delete/{id}', 'ManufacturersController@destroy')->name('destroy');
        Route::post('/create', 'ManufacturersController@store')->name('store');
        Route::get('/create', 'ManufacturersController@create')->name('create');
    });
});
Route::prefix('customer')->group( function () {
    Route::name('customer.')->group( function () {
        Route::get('/', 'CustomerController@index')->name('index');
        Route::get('/edit/{id}', 'CustomerController@edit')->name('edit');
        Route::get('/show/{id}', 'CustomerController@show')->name('show');
        Route::put('/update/{id}', 'CustomerController@update')->name('update');
        Route::get('/delete/{id}', 'CustomerController@destroy')->name('destroy');
        Route::get('/create', 'CustomerController@create')->name('create');
        Route::post('/create', 'CustomerController@store')->name('store');

    });
});
Route::prefix('categorychild')->group( function () {
    Route::name('categorychild.')->group( function () {
        Route::get('/', 'CategoryChildrenController@index')->name('index');
        Route::get('/edit/{id}', 'CategoryChildrenController@edit')->name('edit');
        Route::put('/update/{id}', 'CategoryChildrenController@update')->name('update');
        Route::get('/delete/{id}', 'CategoryChildrenController@destroy')->name('destroy');
        Route::get('/create', 'CategoryChildrenController@create')->name('create');
        Route::post('/create', 'CategoryChildrenController@store')->name('store');

    });
});