<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Smile | Responsive Bootstrap Ecommerce Template</title>

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="/favicon.ico">
    <!-- Google Webfont -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,100,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,100,300,300italic,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" />
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('css/font-awesome/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('js/vendors/isotope/isotope.css')}}">
    <link rel="stylesheet" href="{{asset('js/vendors/slick/slick.css')}}">
    <link rel="stylesheet" href="{{asset('js/vendors/rs-plugin/css/settings.css')}}">
    <link rel="stylesheet" href="{{asset('js/vendors/select/jquery.selectBoxIt.css')}}">
    <link rel="stylesheet" href="{{asset('css/subscribe-better.css')}}">
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/ui-lightness/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('plugin/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('plugin/owl-carousel/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head><!--/head-->

<body>
	@include('frontend.layout.header')
    @yield('content')
    @include('frontend.layout.footer')
    <script src="{{asset('js/jquery.js')}}"></script>

    <!-- ADDTHIS -->
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-557a95e76b3e51d9" async="async"></script>
    <script type="text/javascript">
        // Call this function once the rest of the document is loaded
        function loadAddThis() {
            addthis.init()
        }
    </script>

    <!-- ADDTHIS -->
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-557a95e76b3e51d9" async="async"></script>
    <script type="text/javascript">
        // Call this function once the rest of the document is loaded
        function loadAddThis() {
            addthis.init()
        }
    </script>

    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('plugin/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/bs-navbar.js')}}"></script>
    <script src="{{asset('js/vendors/isotope/isotope.pkgd.js')}}"></script>
    <script src="{{asset('js/vendors/slick/slick.min.js')}}"></script>
    <script src="{{asset('js/vendors/tweets/tweecool.min.js')}}"></script>
    <script src="{{asset('js/vendors/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script src="{{asset('js/vendors/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{asset('js/jquery.sticky.js')}}"></script>
    <script src="{{asset('js/jquery.subscribe-better.js')}}"></script>
    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="{{asset('js/vendors/select/jquery.selectBoxIt.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>
        <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
  
    @stack('scripts')
</body>
</html>