<div class="top_bar">
    @php($categories = App\Category::limit(3)->get())
    <div class="container">
        <div class="row">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="tb_left pull-left">
                        <p>Welcome to our online store !</p>
                    </div>
                    <div class="tb_center pull-left">
                        <ul>
                            <li>
                                <i class="fa fa-phone"></i> Hotline:
                                <a href="#">(+800) 2307 2509 8988</a>
                            </li>
                            <li>
                                <i class="fa fa-envelope-o"></i>
                                <a href="#">online support@smile.com</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tb_right pull-right">
                        <ul>
                            <li>
                                
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- HEADER -->
<header>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://127.0.0.1:8000/"><img src="{{asset('images/basic/logo.png')}}" class="img-responsive" alt="" /></a>
                </div>
                <div class="header-xtra pull-right">
                    <div class="topcart">
                        <span>
                            <i class="fa fa-shopping-cart"></i>
                        </span>
                        <div class="cart-info">
                            <small>You have
                                <em class="highlight">{{\Cart::count()}} item(s)</em> in your
                                shopping bag</small>
                                <?php $total = 0; ?>
                            @foreach(\Cart::content() as $cart)
                                <?php $total += ($cart->qty * $cart->price) ?>
                                <div class="ci-item ciItem-{{$cart->rowId}}">
                                    <img src="{{asset($cart->options[0])}}" width="80" alt="" />
                                    <div class="ci-item-info">
                                        <h5>
                                            <a href="./single-product.html">{{$cart->name}}</a>
                                        </h5>
                                        <p>{{$cart->qty}} x {{number_format($cart->price)}}đ</p>
                                        {{-- <div class="ci-edit">
                                            <a href="#" class="edit fa fa-edit"></a>
                                            <a href="{{route('home.remove',['id'=>$cart->rowId])}}" class="edit fa fa-trash"></a>
                                        </div> --}}
                                    </div>
                                </div>
                            @endforeach
                            <div class="ci-total">Thành tiền: {{number_format($total)}} đ</div>
                            <div class="cart-btn">
                                <a href="{{route('home.cart')}}">Giỏ hàng</a>
                                <a href="{{route('home.checkout')}}">Thanh Toán</a>
                            </div>

                        </div>
                    </div>
                    <div class="topsearch">
                        <span>
                            <i class="fa fa-search"></i>
                        </span>
                        <form method="POST" action="{{route('home.searchpost')}}" class="searchtop">
                            @csrf
                            <input type="text" name="search" placeholder="Search entire store here." />
                        </form>
                    </div>
                </div>
                <!-- Navmenu -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown mmenu">
                            <a href="./categories-grid.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Sản Phẩm</a>
                            <ul class="mega-menu dropdown-menu" role="menu">
                                @foreach($categories as $category)
                                    <li>
                                        <div>
                                        <a href="{{route('home.products',['id'=>$category->id])}}"><h5>{{$category->name}}</h5></a>
                                            @foreach($category->categorychild as $categorychildItem)
                                                <a href="{{route('home.products',['id'=>$categorychildItem->id])}}">{{$categorychildItem->name}}</a>
                                            @endforeach
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="./categories-grid.html" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Shop</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Blog</a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Pages</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>
