@extends('frontend.layout.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="check-out col-lg-7">
            @foreach ($errors->all() as $message)
             <li>{{$message}}</li>
            @endforeach
            <form method="post" action="{{ route('home.order_detail')}}">
                @csrf
                @method('POST')
                <div class="form-group">
                    <label for="email">Name:</label>
                    <input type="text" class="form-control"  placeholder="Enter name" name="name">
                </div>
                <div class="form-group">
                    <label for="pwd">Phone:</label>
                    <input type="text" class="form-control"  placeholder="Enter phone" name="phone">
                </div>
                <div class="form-group">
                    <label for="pwd">Address:</label>
                    <input type="text" class="form-control"  placeholder="Enter address" name="address">
                </div>
                <div class="form-group">
                   <label for="pwd">Note:</label>
                   <input type="text" class="form-control"  placeholder="Enter address" name="note">
               </div>
                <div class="form-group form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="remember"> Remember me
                    </label>
                </div>
                <button  type="submit" class="btn btn-primary">Xac nhan mua hang</button>
            </form>
        </div>
            <div class="col-lg-5">
                <h2 class="title-secres">Giỏ Hàng</h2>
                <ul class="list-unstyled">
                    @foreach (\Cart::content() as $item)
                    <li>
                        <figure>
                            <a class="cart_list_product_img" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body">
                                <img src="{{$item->options[0]}}" alt="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body" class="img-reponsive imds">
                            </a>
                        </figure>
                        <div class="list_content">
                            <h5>
                                <span class="name-ps">{{$item->name}}</span>
                           </h5>
                        <div class="quantity">
                                {{$item->qty}} x
                                <span class="amount">
                                    <span class="money" style="color: #199427; font-size: 14px !important;">
                                    {{number_format($item->price)}} ₫
                                            </span>
                               </span>
                                        <div class="pull-right tt">
                                {{number_format($item->qty * $item->price)}}₫
                                   </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    
                </ul>
              
                <div class="load-lead">
                    <div class="box-coupon">
                        <p class="list-info-price" id="price-before" data-id="318000">
                            <small class="color-black">Tạm tính: </small> <span style="color:green">{{number_format($total),0,'.',','}} ₫</span>
                        </p>
                    </div>
                </div>
                <div class="load-ship">
                    <div class="box-coupon">
                        <p class="list-info-price" id="priceship" data-id="0">
                            <small class="color-black">Phí vận chuyển: </small> <span>Miễn phí!</span>
                        </p>
                    </div>
                </div>
                <div class="subtotal">
                    Thành tiền:
                    <span>
                        <span class="amount">
                            <strong class="money lucal" style="color: #199427;"> {{number_format($total),0,'.',','}} ₫ </strong>
                        </span>
                    </span>
                </div>
                <div class="pttt">
                    <span>* Phương thức thanh toán: Nhận hàng &amp; thanh toán tiền mặt tại nhà</span>
                </div>
            </div>
        </div>
</div>

@endsection
