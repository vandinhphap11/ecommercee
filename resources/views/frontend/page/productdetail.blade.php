@extends('frontend.layout.app')
@section('content')
<div class="container">
    <div class="row">
        {{-- <form method="POST" action="{{route('home.addtocart')}}"> --}}
            @csrf
            <input type="hidden" name="product_id" value="{{$product->id}}">
            <div class="col-md-5 col-sm-6">
                <img width="100%" src="{{asset('storage/'.str_replace('public','',$product->image))}}" alt="">
            </div>
            <div class="col-md-7 col-sm-6">
                <div class="product-single">
                    <div class="ps-header">
                        <h3>{{$product->name}}</h3>
                        <div class="ps-price"><span>$ 200.00</span> {{($product->price)}}đ</div>
                    </div>
                    <div class="space10"></div>
                  
                    <div class="card-box">
                        <div class="hide  min-sch ">
                            <span class="">: <span><b id="load-type">không có gì</b></span></span>
                        </div>
                        <div class="select-land hide ">
                            <span class="">:</span>
                            <ul class="ul-non ul-select">
                            </ul>
                            <div class="clr"></div>
                        </div>
                        <div class="inner-load">
                            <div class="box-add">
                                <div class="number-card">
                                    <span>Số lượng:</span>
                                    <input class="cart_quantity_input" type="number" name="quantity" value="1" autocomplete="off" size="2" min="1" max="10">
                                    <div class="clr"></div>
                                </div>
                                <div class="bst">
                                    <a href="javascript:;" data-addcart="{{$product->id}}" class="addcart">
                                    <button type="button" class="btn-bts btn-atc">
                                        <i class="fa fa-shopping-cart"></i>
                                        Thêm giỏ hàng
                                    </button>
                                    </a>
                                    <div class="clr"></div>
                                </div>
                                <div class="clr"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@push('scripts')
    <script>
        $('.addcart').on('click', function(){
            var data = $(this).data('addcart');
            var qty = $('input[name="quantity"]').val();
            $.ajax({
                type: "POST",
                url: "/addToCart",
                data: {dataID:data,qty:qty},
                success: function (res) {
                    console.log(res);
                    let html = `<small>You have
                                <em class="highlight">${res.data.count} item(s)</em> in your
                                shopping bag</small>`;
                    if(res.statusCode == 200){
                        $.each(res.data.cart, function(i,v){
                            html += `
                            <div class="ci-item">
                                    <img src="/${v.options[0]}" width="80" alt="" />
                                    <div class="ci-item-info">
                                        <h5>
                                            <a href="./single-product.html">${v.name}</a>
                                        </h5>
                                        <p>${v.qty} x ${v.price}đ</p>
                                    </div>
                                </div>
                            `
                        })
                        html +=`
                            <div class="ci-total">Thành tiền: ${res.data.total} đ</div>
                            <div class="cart-btn">
                                <a href="/cart">Giỏ Hàng</a>
                                <a href="/checkout">Thanh Toán</a>
                            </div>
                        `
                        $('.cart-info').html(html);
                        toastr('thành công','đã thêm vào giỏ hàng thành công','success'); 
                    }else{
                        toastr('không thành công','gặp 1 số lỗi vui lòng tải lại trang','warning'); 
                    }

                }
            });
        });
        
        function toastr(title,mess,icon){
            $.toast({
                heading: title,
                text: mess,
                icon: icon,
                loader: true,      
                position: 'bottom-right', 
                loaderBg: '#9EC600'
            })
        }
    </script>
@endpush
@endsection
