@extends('frontend.layout.app')
@section('content')
   
@php($category = \App\Category::all())
@php($brand = \App\Manufacturers::all())
    <div class="shop-content">
    <div class="container">
        <div class="row">
            <aside class="col-md-3 col-sm-4">
                
                <div class="side-widget">
                    <h3><span>Shop by</span></h3>
                    <h5>Categories</h5>
                    <ul class="cat-list">
                        @foreach ($category as $item)
                            <li><a href="{{route('home.products',['id'=>$item->id])}}">{{$item->name}}</a></li>
                        @endforeach
                       
                    </ul>
                    <div class="clearfix space20"></div>
                    <h5>Price Slider</h5>
                    <div id="slider-container"></div>
                    <p>
                        <a href="#" class="btn-black pull-left">Filter Now</a>
                        <span class="pull-right sc-range">
                                <label class="range-label" for="amount">Price:</label>
                                <input type="text" id="amount" style="border: 0; color: #333333; font-weight: bold;" />
                            </span>
                    </p>
                    <div class="clearfix space30"></div>
                    <h5>Manufactures</h5>
                    <ul class="brand-list">
                        @foreach($brand as $manufactures)
                            <li><a href="{{route('home.brands',['id'=>$manufactures->id])}}">{{$manufactures->name}}</a></li>
                        @endforeach
                    </ul>
                    <div class="clearfix space20"></div>
                    <h5>Color Options</h5>
                    <ul class="color-list">
                        <li><a href="#"><span class="black"></span>Black</a></li>
                        <li><a href="#"><span class="darkgrey"></span>Grey</a></li>
                        <li><a href="#"><span class="red"></span>Red</a></li>
                        <li><a href="#"><span class="liteblue"></span>Blue</a></li>
                        <li><a href="#"><span class="yellow"></span>Yellow</a></li>
                        <li><a href="#"><span class="brown"></span>Brown</a></li>
                    </ul>
                    <div class="clearfix space20"></div>
                    <h5>Size Options</h5>
                    <ul class="size-list">
                        <li><a href="#">XS</a></li>
                        <li><a href="#">S</a></li>
                        <li><a href="#">M</a></li>
                        <li><a href="#">L</a></li>
                        <li><a href="#">XL</a></li>
                        <li><a href="#">XXL</a></li>
                    </ul>
                </div>
                <div class="clearfix space30"></div>
                <div class="side-widget">
                    <h3><span>Community Polls</span></h3>
                    <form class="poll">
                        <p>What is your favourite fashion style ?</p>
                        <div class="space20"></div>
                        <input type="radio" name="radio_poll" id="radio1" class="css-checkbox" /><label for="radio1" class="css-label radGroup1">Classic Style</label><br>
                        <input type="radio" name="radio_poll" id="radio2" class="css-checkbox" checked="checked" /><label for="radio2" class="css-label radGroup1">Bohrmian Style</label><br>
                        <input type="radio" name="radio_poll" id="radio3" class="css-checkbox" /><label for="radio3" class="css-label radGroup1">Sreet Style</label><br>
                        <input type="radio" name="radio_poll" id="radio4" class="css-checkbox" /><label for="radio4" class="css-label radGroup1">elegant Style</label><br>
                        <button type="submit">Vote</button>
                    </form>
                </div>
                <div class="space30"></div>
                <div class="side-widget">
                    <h3><span>Compare Products</span></h3>
                    <div class="compare-wrap">
                        <p>You have no item(s) to compare</p>
                    </div>
                </div>
            </aside>
            <div class="col-md-9 col-sm-8">
                <div class="filter-wrap">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            View as: <span><a class="active">Grid</a> / <a href="./categories-list.html">List</a></span>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            Sort by:
                            <select>
                                    <option>Default</option>
                                    <option>Newest</option>
                                    <option>Popular</option>
                                    <option>Recently Sold</option>
                                </select>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <span class="pull-right">
                                    Show:
                                    <select>
                                        <option>9 items</option>
                                        <option>18 items</option>
                                        <option>27 items</option>
                                        <option>50 items</option>
                                    </select>
                                </span>
                        </div>
                    </div>
                </div>
                <div class="space50"></div>
                <div class="row">
                    <p>{{$data['message']}}</p>
                    @if (count($data['search']) > 0)
                    @foreach ($data['search'] as $product)
                    <div class="col-md-4 col-sm-6">
                        <div class="product-item">
                            <div class="item-thumb">
                                <span class="badge new">New</span>
                                <img src="{{asset('storage/'.str_replace('public','',$product->image))}}" class="img-responsive" alt="" />
                                <a class="overlay-rmore fa fa-search quickview" href="{{route('home.detail',['id'=>$product->id])}}"></a>
                                <div class="product-overlay">
                                    <a href="#" class="addcart fa fa-shopping-cart"></a>
                                    <a href="#" class="compare fa fa-signal"></a>
                                    <a href="#" class="likeitem fa fa-heart-o"></a>
                                </div>
                            </div>
                            <div class="product-info">
                                <h4 class="product-title"><a href="./single-product.html">{{$product->name}}</a></h4>
                                <span class="product-price">${{$product->price}} <em>- Pre order</em></span>
                                <div class="item-colors">
                                    <a href="#" class="litebrown"></a>
                                    <a href="#" class="white"></a>
                                    <a href="#" class="red"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
                </div>
                
            </div>
        </div>
        <div class="space50"></div>
    </div>
</div>
@endsection