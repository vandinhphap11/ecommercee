@extends('frontend.layout.app')
@section('content')
<div class="container">
   <div class="row">
    <div class="col-lg-8 col-md-12 cart-col-1">
        <div id="load-error">
        </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-12" style="padding-left:0; height:31px">
                    <span class="cart_index">Giỏ hàng <span>(2 sản phẩm)</span></span>
                    <span class="badge"></span>
                </div>
                <div class="col-lg-2 col-md-2 hidden-xs">
                    <h6>&nbsp; Giá mua</h6>
                </div>
                <div class="col-lg-2 col-md-2 hidden-xs">
                    <h6> Số lượng</h6>
                </div>
                <div class="col-lg-2 col-md-2 hidden-xs">
                    <h6> Thành tiền</h6>
                </div>
            </div>
                @php
                   $total = 0;
                @endphp
                @forelse(\Cart::content() as $c)
                @php
                    $total +=($c ->qty * $c->price)
                @endphp
                <div class="shopping-cart-item cartItem-{{$c->rowId}}">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-3 img-thumnail-custom">
                            <div class="image">
                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="Tẩy Da Chết Toàn Thân The Face Shop Smooth Body ">
                                    <img class="img-reponsive" src="{{asset('storage/'. str_replace('public/', '',\App\Product::find($c->id)->image))}}" alt="{{$c->name}}">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 c2 col-6">
                            <div class="name">
                                <a class="top_zindex" href="/tay-da-chet-toan-than-the-face-shop-smooth-body-peel-300ml.html" title="{{$c->name}}"> {{$c->name}}</a>
                            </div>
                            <div class="list-endow">
                                <span class="gift js-product-gift-icon hidden-sm hidden-xs">
                                    <img src="https://vcdn.tikicdn.com/assets/img/icon-svg/icon-gift.svg" class="imgquatang">
                                </span>
                                <img src="https://vcdn.tikicdn.com/assets/img/icon-svg/icon-24h.svg" class="img24h hidden-sm hidden-xs" alt="Hỗ trợ giao hàng trong 24h tại TPHCM" title="Hỗ trợ giao hàng trong 24h tại TPHCM">
                                <span class="boxflag hide">
                                    :
                                </span>
                            </div>
                            <p id="product-status-8809115025487">
                            </p>
                            <p class="action">
                                <a href="javascript:;" data-remove_id="{{$c->rowId}}" class="btn btn-link btn-item-delete hidden-sm hidden-xs" title="Xóa"> Xóa </a>
                                </p><div class="clr"></div>
                            <p></p>
                        </div>
                        <div class="col-lg-2 col-md-2 col-3" style="position: inherit!important;">
                            <div class="mar-bot-mb">
                                <span class="price price-mb" style="color: #199427;">{{number_format($c->price)}}đ</span>
                            </div>
                        </div>
                        <div class="col-3 hidden-lg hidden-md"></div>
                        <div class="col-lg-2 col-md-2 col-6 ">
                            <div class="form-group">
                                <div class="input-group wb-60">
                                    <input  class="btn-qty" type="number" data-product_id="{{$c->rowId}}"  name="quantity" value="{{$c->qty}}" maxlength="2" min="1" max="10" size="1" id="number" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-3">
                            <p class="sub-price sub-price-{{$c->rowId}} tt-mb" style="color: #199427;"> {{number_format($c->price * $c->qty)}}đ</p>
                        </div>
                        <div class="box-info-discount-mobile hidden-md hidden-lg"></div>
                    </div>
                </div>
                @empty
                @endforelse
                
            </div>
       <div class="col-lg-4 col-md-12">
        <div class="panel panel-default fee">
            <div class="panel-body">
                <p class="list-info-price" id="pricetotal" data-id="260000">
                    <b>Tạm tính: </b> <span class="total-price">{{number_format($total)}}đ</span>
                </p>
                <div class="load-giamgia hide">
                </div>
                <p class="total2">Thành tiền: <span style="float: right" class="total-price load-price">{{number_format($total)}}đ </span> </p>
                <p class="text-right">
                    <i>(Chưa bao gồm phí vận chuyển)</i>
                </p>
            </div>
        </div>
            <a href="{{route('home.checkout')}}"><button id="btn_submit_cart" type="button" class="btn btn-large btn-block btn-default btn-checkout">TIẾN HÀNH ĐẶT HÀNG </button></a>
        </div>
   </div>
</div>
</div>
@push('scripts')
<script>

    $('.btn-qty').on('keyup mouseup', function(e){
        var productid = $(this).data('product_id');
        var num = $(this).val();
        if(num < 0) {
            $(this).val(1);
        }
        updateCartItem(productid,num);
    });
    function updateCartItem(productid,num){
        $.ajax({
                type: "POST",
                url: "/updateCart",
                data: {dataID:productid,qty:num},
                // beforeSend: function() {
                //     $('.btn-qty').attr("disabled", true);
                // },
                success: function (res) {
                    if(res.status == 200) {
                        console.log(res);
                        $(`.sub-price-${res.data.cart.rowId}`).html(res.data.subPrice);
                        $('.total-price').html(res.data.total);
                    } 
                    // $('.btn-qty').attr("disabled", false);  
                }
            });
    }
    $('.btn-item-delete').on('click',function(){
        var removeid = $(this).data('remove_id');
        console.log(removeid);
        removeCartItem(removeid)
    });
    function removeCartItem(removeid){
        $.ajax({
            type: "POST",
            url: "/removeCart",
            data: {dataID:removeid},
            success: function (res) {
                $(`.cartItem-${removeid}`).remove();
                $(`.ciItem-${removeid}`).remove();
                $('.ci-total').html(res.total);
                $('.highlight').html(res.count);
                toastr('thành công','đã xoá sản phẩm','success');
            }
        });
    }
    function toastr(title,mess,icon){
            $.toast({
                heading: title,
                text: mess,
                icon: icon,
                loader: true,      
                position: 'bottom-right', 
                loaderBg: '#9EC600'
            })
        }
</script>
@endpush
<style>
    .topcart {
        display:none;
    }
</style>
@endsection
