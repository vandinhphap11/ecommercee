@extends('frontend.layout.app')
@section('content')
<div class="slider-wrap">
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-thumb="homeslider_thumb1.jpeg" data-saveperformance="on" data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('images/slides/1.jpeg')}}" alt="slidebg1" data-lazyload="images/slides/2.jpeg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <div class="tp-caption customin fadeout tp-resizeme rs-parallaxlevel-10" data-x="center" data-y="center" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-speed="800" data-start="1000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="300" style="z-index: 2; max-width: 630px; max-height: 250px; background:#fff;width:100%;height:100%; white-space: nowrap;">
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="250" data-speed="1000" data-start="1400" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;font-family: Raleway;
                             font-size: 36px;
                             font-weight: bold;
                             text-transform: uppercase;	color: #343434;">Amazing <span class="ss-color" style="color:#d6644a;">Outlet</span></div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="310" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;	font-family: Raleway;
                             font-size: 18px;
                             color: #333;text-align:center;">
                        Clean & Elegant design with a modern style. This template includes<br> all you need for a fashion & accessories store
                    </div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="375" data-speed="1000" data-start="2200" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: 80px; max-height: 4px; width:100%;height:100%;background:#000000;"></div>
                    <a href="./categories-grid.html" class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="395" data-speed="1000" data-start="2600" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300"
                        data-endspeed="1000" style="z-index: 3; max-height:100%;line-height:43px;color:#fff;font-family: Montserrat;
                           font-size: 12px;
                           display:table;
                           font-weight: bold;
                           text-transform:uppercase;padding:0 40px;background:#000000;position:relative;z-index:77;">
                            Shop Now !
                        </a>
                </li>
                <li data-transition="fade" data-slotamount="2" data-masterspeed="500" data-thumb="homeslider_thumb1.jpeg" data-saveperformance="on" data-title="Intro Slide">
                    <!-- MAIN IMAGE -->
                    <img src="{{asset('images/slides/2.jpeg')}}" alt="slidebg1" data-lazyload="images/slides/1.jpeg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <div class="tp-caption customin fadeout tp-resizeme rs-parallaxlevel-10" data-x="center" data-y="center" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                        data-speed="800" data-start="1000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="300" style="z-index: 2; max-width: 630px; max-height: 250px; background:#fff;width:100%;height:100%; white-space: nowrap;">
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="250" data-speed="1000" data-start="1400" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;font-family: Raleway;
                             font-size: 36px;
                             font-weight: bold;
                             text-transform: uppercase;	color: #343434;">Women <span class="ss-color" style="color:#d6644a;">Clothing</span>
                    </div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="310" data-speed="1000" data-start="1800" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;	font-family: Raleway;
                             font-size: 18px;
                             color: #333;text-align:center;">
                        Clean & Elegant design with a modern style. This template includes<br> all you need for a fashion & accessories store
                    </div>
                    <div class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="375" data-speed="1000" data-start="2200" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300" data-endspeed="1000" style="z-index: 3; max-width: 80px; max-height: 4px; width:100%;height:100%;background:#000000;">
                    </div>
                    <a href="./categories-grid.html" class="tp-caption lft skewtoleftshort rs-parallaxlevel-9" data-x="center" data-y="395" data-speed="1000" data-start="2600" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-end="7300"
                        data-endspeed="1000" style="z-index: 3; max-height:100%;line-height:43px;color:#fff;font-family: Montserrat;
                           font-size: 12px;
                           display:table;
                           font-weight: bold;
                           text-transform:uppercase;padding:0 40px;background:#000000;position:relative;z-index:77;">
                            Shop Now !
                        </a>
                </li>
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</div>
<div class="block-main container">
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <div class="block-content">
                <img src="images/blocks/1.jpeg" class="img-responsive" alt="" />
                <div class="bs-text-down text-center hvr-outline-out">
                    Menswear<span>Intimates Fall/Winter 2015</span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="block-content">
                <img src="images/blocks/2.jpeg" class="img-responsive" alt="" />
                <div class="bs-text-center text-center">
                    Accesories<span>Get a new look with Smile Collection</span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="block-content">
                <img src="images/blocks/3.jpeg" class="img-responsive" alt="" />
                <div class="bs-text-down text-center">
                    Womenswear<span>Smile Collection new arrivals</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- FEATURED PRODUCTS -->
<div class="featured-products">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h5 class="heading"><span>Featured Products</span></h5>
                <ul class="filter" data-option-key="filter">
                    <li><a class="dataCategory selected" data-category_id ="*" href="javascript:;">All</a></li>
                    @foreach ($categories as $item)
                        <li><a class="dataCategory" data-category_id ="{{$item->id}}" href="javascript:;">{{$item->name}}</a></li>
                    @endforeach
                </ul>
                <div id="isotope" class="isotope">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<!-- POLICY -->
<div class="policy-item parallax-bg1">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="pi-wrap text-center">
                    <i class="fa fa-plane"></i>
                    <h4>Free shipping<span>Free shipping on all UK order</span></h4>
                    <p>Nulla ac nisi egestas metus aliquet euismod. Sed pulvinar lorem at pretium.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="pi-wrap text-center">
                    <i class="fa fa-money"></i>
                    <h4>Money Guarantee<span>30 days money back guarantee !</span></h4>
                    <p>Curabitur ornare urna enim, et lacinia purus tristique eulla eget feugiat diam.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="pi-wrap text-center">
                    <i class="fa fa-clock-o"></i>
                    <h4>Store Hours<span>Open: 9:00AM - Close: 21:00PM</span></h4>
                    <p>Etiam egestas purus eget sagittis lacinia. Morbi vel elit nec eros iaculis.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="pi-wrap text-center">
                    <i class="fa fa-life-ring"></i>
                    <h4>Support 24/7<span>We support online 24 hours a day</span></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit integer congue.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="top-product">
    <div class="container">
        <div class="row">
            <div class="body-box-head">
                <h5 class="heading"><span>Sản Phẩm Bán Chạy Nhất</span></h5>
            </div>
            <div class="body-box-category">
                @foreach ($topProduct as $topproduct)
                    <div class="col-lg-2 col-md-3 topproduct">
                        <div class="box-product">
                            <div class="box-images">
                                <a href="#"><img class="img-reponsive" src="{{asset('storage/'.str_replace('public','',$topproduct->image))}}" alt=""></a>
                            </div>
                            <div class="box-content">
                                <h3>
                                    <a href="{{route('home.detail',['id'=>$topproduct->id])}}">
                                    <p>{{$topproduct->name}}</p>
                                </a>
                                </h3>
                                <div>
                                    <span class="price">{{number_format($topproduct->price)}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
           
        </div>
    </div>
</div>
<!-- BLOG -->
<div class="home-blog">
    <div class="container">
        <h5 class="heading space40"><span>Latest from our blog</span></h5>
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <article class="home-post text-center">
                    <div class="post-thumb">
                        <a href="./blog-single.html">
                            <img src="images/blog/1/1.jpeg" class="img-responsive" alt="" />
                            <div class="overlay-rmore fa fa-link"></div>
                        </a>
                    </div>
                    <div class="post-excerpt">
                        <h4><a href="./blog-single.html">A Beautiful & Creative design</a></h4>
                        <div class="hp-meta">
                            <span><i class="fa fa-edit"></i> September 25, 1989</span>
                            <span><i class="fa fa-comment-o"></i> <a href="#">2037 Comments</a></span>
                        </div>
                        <p>Fusce id viverra leo, quis sollicitudin risus. Sed maximus dictum semper. Sed laoreet euismod dui [..]</p>
                    </div>
                </article>
            </div>
            <div class="col-md-4 col-sm-4">
                <article class="home-post text-center">
                    <div class="post-thumb">
                        <a href="./blog-single.html">
                            <img src="images/blog/1/2.jpeg" class="img-responsive" alt="" />
                            <div class="overlay-rmore fa fa-link"></div>
                        </a>
                    </div>
                    <div class="post-excerpt">
                        <h4><a href="./blog-single.html">A Beautiful & Creative design</a></h4>
                        <div class="hp-meta">
                            <span><i class="fa fa-edit"></i> September 25, 1989</span>
                            <span><i class="fa fa-comment-o"></i> <a href="#">2037 Comments</a></span>
                        </div>
                        <p>Fusce id viverra leo, quis sollicitudin risus. Sed maximus dictum semper. Sed laoreet euismod dui [..]</p>
                    </div>
                </article>
            </div>
            <div class="col-md-4 col-sm-4">
                <article class="home-post text-center">
                    <div class="post-thumb">
                        <a href="./blog-single.html">
                            <img src="images/blog/1/3.jpeg" class="img-responsive" alt="" />
                            <div class="overlay-rmore fa fa-link"></div>
                        </a>
                    </div>
                    <div class="post-excerpt">
                        <h4><a href="./blog-single.html">A Beautiful & Creative design</a></h4>
                        <div class="hp-meta">
                            <span><i class="fa fa-edit"></i> September 25, 1989</span>
                            <span><i class="fa fa-comment-o"></i> <a href="#">2037 Comments</a></span>
                        </div>
                        <p>Fusce id viverra leo, quis sollicitudin risus. Sed maximus dictum semper. Sed laoreet euismod dui [..]</p>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>

<!-- random ARRIVALS -->
<div class="container padding40">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h5 class="heading space40"><span>Random Products</span></h5>
            <div class="product-carousel3">
                @foreach ($productRandom as $productrandom)
                {{-- {{dd($productrandom)}} --}}
                <div class="pc-wrap">
                    <div class="product-item">
                        <div class="item-thumb">
                            <img src="{{asset('storage/'.str_replace('public','',$productrandom->image))}}" class="img-responsive" alt="" />
                            <a class="overlay-rmore fa fa-search quickview" href="{{route('home.detail',['id'=>$productrandom->id])}}"></a>
                            <div class="product-overlay">
                                {{-- <a href="javascript:;" data-addcart="{{$productrandom->id}}" class="addcart fa fa-shopping-cart"></a> --}}
                            </div>
                        </div>
                        <div class="product-info">
                            <h4 class="product-title"><a href="{{route('home.detail',['id'=>$productrandom->id])}}">{{$productrandom->name}}</a></h4>
                            <span class="product-price">{{number_format($productrandom->price)}}đ</span>
                            
                        </div>
                    </div>
                </div>
                @endforeach
               
            </div>
        </div>
    </div>
</div>

<div class="space30 clearfix"></div>

<!-- TESTIMONIAL -->
<div class="testimonial parallax-bg2">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="quote-carousel">
                    <div>
                        <img src="images/quote/1.png" class="img-responsive" alt="" />
                        <div class="quote-info">
                            <h4>Smile Nguyen</h4>
                            <cite>Themeforest</cite>
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris convallis odio in faucibus posuere. In eu scelerisque lorem. Mauris lusto in lacus accumsan interdum.Nam mattis sollicitudin vestibulum"</p>
                        </div>
                    </div>
                    <div>
                        <img src="images/quote/2.png" class="img-responsive" alt="" />
                        <div class="quote-info">
                            <h4>Smile Nguyen</h4>
                            <cite>Themeforest</cite>
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris convallis odio in faucibus posuere. In eu scelerisque lorem. Mauris lusto in lacus accumsan interdum.Nam mattis sollicitudin vestibulum"</p>
                        </div>
                    </div>
                    <div>
                        <img src="images/quote/3.png" class="img-responsive" alt="" />
                        <div class="quote-info">
                            <h4>Smile Nguyen</h4>
                            <cite>Themeforest</cite>
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris convallis odio in faucibus posuere. In eu scelerisque lorem. Mauris lusto in lacus accumsan interdum.Nam mattis sollicitudin vestibulum"</p>
                        </div>
                    </div>
                    <div>
                        <img src="images/quote/3.png" class="img-responsive" alt="" />
                        <div class="quote-info">
                            <h4>Smile Nguyen</h4>
                            <cite>Themeforest</cite>
                            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris convallis odio in faucibus posuere. In eu scelerisque lorem. Mauris lusto in lacus accumsan interdum.Nam mattis sollicitudin vestibulum"</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- LATEST PRODUCTS -->
<div class="container padding40">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h5 class="heading space40"><span>Latest Products</span></h5>
            <div class="product-carousel3">
                @foreach ($newProducts as $new)
                <div class="pc-wrap">
                    <div class="product-item">
                        <div class="item-thumb">
                            <img src="{{asset('storage/'.str_replace('public','',$new->image))}}" class="img-responsive" alt="" />
                            <a class="overlay-rmore fa fa-search quickview" href="{{route('home.detail',['id'=>$new->id])}}"></a>
                            <div class="product-overlay">
                                {{-- <a href="javascript:;" data-addcart="{{$productrandom->id}}" class="addcart fa fa-shopping-cart"></a> --}}
                            </div>
                        </div>
                        <div class="product-info">
                            <h4 class="product-title"><a href="./single-product.html">{{$new->name}}</a></h4>
                            <span class="product-price">{{number_format($new->price)}}đ</span>
                           
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="space10 clearfix"></div>

<!-- CLIENTS -->
<div class="clients">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="clients-carousel">
                    <div>
                        <a href="#"><img src="images/clients/1.png" class="img-responsive" alt="" /></a>
                    </div>
                    <div>
                        <a href="#"><img src="images/clients/2.png" class="img-responsive" alt="" /></a>
                    </div>
                    <div>
                        <a href="#"><img src="images/clients/3.png" class="img-responsive" alt="" /></a>
                    </div>
                    <div>
                        <a href="#"><img src="images/clients/4.png" class="img-responsive" alt="" /></a>
                    </div>
                    <div>
                        <a href="#"><img src="images/clients/5.png" class="img-responsive" alt="" /></a>
                    </div>
                    <div>
                        <a href="#"><img src="images/clients/6.png" class="img-responsive" alt="" /></a>
                    </div>
                    <div>
                        <a href="#"><img src="images/clients/1.png" class="img-responsive" alt="" /></a>
                    </div>
                    <div>
                        <a href="#"><img src="images/clients/2.png" class="img-responsive" alt="" /></a>
                    </div>
                    <div>
                        <a href="#"><img src="images/clients/3.png" class="img-responsive" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER WIDGETS -->
<div class="f-widgets">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <h6>Best Seller</h6>
                <div class="f-widget-content">
                    <ul>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/fashion/1.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/fashion/9.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/fashion/7.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <h6>Best Offer</h6>
                <div class="f-widget-content">
                    <ul>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/fashion/4.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/fashion/10.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/fashion/8.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <h6>Recent Products</h6>
                <div class="f-widget-content">
                    <ul>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/accessories/1.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/fashion/10.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/accessories/11.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <h6>Top Rated</h6>
                <div class="f-widget-content">
                    <ul>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/fashion/2.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <div class="ratings">
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                </div>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/fashion/18.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <div class="ratings">
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                </div>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                        <li>
                            <div class="fw-thumb">
                                <img src="images/products/accessories/13.jpeg" alt="" />
                            </div>
                            <div class="fw-info">
                                <h4><a href="./single-product.html">Product fashion</a></h4>
                                <div class="ratings">
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                    <span class="act fa fa-star"></span>
                                </div>
                                <span class="fw-price">$ 99.00</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(document).ready(function(){

        var data = $('.dataCategory');

        data.on('click', function(){
            var categoryid = $(this).data('category_id');
            console.log(categoryid);
            loadTabProductSelected(categoryid);
        })
        loadTabProductSelected('*')
        
        function loadTabProductSelected(categoryid){
            let html = '';
            $.ajax({
                url:'/product/tab/' + categoryid,
                type: 'get',
                success:function(data){
                    $.each(data,function(i,v){
                        html += `
                        <div class="col-2-ct">
                            <div class="isotope-item">
                                <div class="product">
                                    <div class="item-thumb">
                                    <a href="/detail/${v.id}"><img class="image-product" src="/${v.image}" class="img-responsive"/></a>  
                                        <a class="overlay-rmore fa fa-search quickview" href="/detail/${v.id}"></a>
                                    </div>
                                    <div class="product-info">
                                        <h4 class="product-title"><a href="/detail/${v.id}">${v.name}</a></h4>
                                        <span class="product-price">${v.price}đ</em></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                            `
                    })
                    $('#isotope').html(html);
                }
            })
        }
    
        $('.filter li a').click(function () {
            $('.filter li a').removeClass('selected');
            $(this).addClass('selected');
        });

        $('.addcart').on('click', function(){
            var data = $(this).data('addcart');
            $.ajax({
                type: "POST",
                url: "/addToCart",
                data: {dataID:data},
                success: function (res) {
                    let html = `<small>You have
                                <em class="highlight">${res.data.count} item(s)</em> in your
                                shopping bag</small>`;
                    if(res.status == 200){
                        $.each(res.data.cart, function(i,v){
                            html += `
                                <div class="ci-item">
                                    <img src="${v.options[0]}" width="80" alt="" />
                                    <div class="ci-item-info">
                                        <h5>
                                            <a href="./single-product.html">${v.name}</a>
                                        </h5>
                                        <p>${v.qty} x ${v.price}đ</p>
                                    </div>
                                </div>
                            `
                        })
                        html +=`
                            <div class="ci-total">Thành tiền: ${res.data.total}đ</div>
                            <div class="cart-btn">
                                <a href="/cart">View Bag</a>
                            </div>
                        `
                        $('.cart-info').html(html);
                    }
                }
            });
        });
    })
</script>
@endpush
@endsection
