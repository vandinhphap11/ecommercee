@extends('adminlte::page')
@section('title', 'Danh sach the loai')
@section('content')
    <h1 style="text-align: center">List Model Machine</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Stt</th>
            <th scope="col">Name</th>
            <th scope="col">Category</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php  $stt = 1;?>
        @forelse($catechildren as $catechildren)
            <tr>
                <th scope="row">{{ $stt }}</th>
                <td><a href="{{route('categorychild.edit',['id' => $catechildren->id])}}">{{$catechildren->name }}</a></td>
                <td>{{ $catechildren->category->name }}</td>
                <td><a href="{{ route('categorychild.destroy',['id' => $catechildren->id]) }}">Delete</a></td>
            </tr>
            <?php $stt++;?>
        @empty
        @endforelse
        </tbody>
    </table>
@stop

