@extends('adminlte::page')

@section('title', 'Tao san pham moi')
@section('content')
    <h1 style="text-align: center">Create Products</h1>
    <h1>{{ (session('message') ? session('message') : " ") }}</h1>
    <div class="error">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <form method="post" action="{{ route('product.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" value="{{ old('name') }}" name="name" class="form-control"  aria-describedby="emailHelp" placeholder="ENTER NAME">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Desc</label>
            <input type="text" value="{{ old('description') }}" name="description" class="form-control"  placeholder="Description">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Price</label>
            <input type="text" value="{{ old('price') }}" name="price" class="form-control"   placeholder="Price">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Image</label>
            <input type="file" name="image" class="form-control-file">
        </div>
        <div class="form-group">
            <input type="hidden" name="checkbox" value="0">
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Category Child</label>
            <select name="categorychildren_id" class="form-control">
                @forelse($categorychild as $cate)
                <option value="{{ $cate->id }}">{{ $cate->name }}</option>
                @empty
                    <option>No data</option>
                @endforelse
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Manufacturers</label>
            <select name="manufacturers_id" class="form-control">
                @forelse($manufacturers as $manu)
                    <option value="{{ $manu->id }}">{{ $manu->name }}</option>
                @empty
                    <option>No data</option>
                @endforelse
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlFile1">Category</label>
            <select name="category_id" class="form-control">
                @forelse($category as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @empty
                    <option>No data</option>
                @endforelse
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </form>
@stop
@section('js')
    
@endsection
