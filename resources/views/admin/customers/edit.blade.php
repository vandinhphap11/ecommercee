@extends('adminlte::page')

@section('title', 'Tao san pham moi')
@section('content')
    <h1 style="text-align: center">Edit Product</h1>
    <h1>{{ (session('message') ? session('message') : " ") }}</h1>
    <div class="error">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <form method="post" action="{{ route('customer.update',['id'=>$customer->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="exampleInputEmail1">Họ Và Tên</label>
            <input type="text" value="{{$customer->name}}" name="name" class="form-control"  aria-describedby="emailHelp" placeholder="Họ Và Tên">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Địa chỉ</label>
            <input type="text" value="{{$customer->address}}" name="address" class="form-control"  placeholder="Địa Chỉ">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Quận/Huyện</label>
            <input type="text" value="{{$customer->dictrict}}" name="dictrict" class="form-control"   placeholder="Quận/Huyện">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Thành Phố</label>
            <input type="text" value="{{$customer->city}}" name="city" class="form-control"  aria-describedby="emailHelp" placeholder="Thành Phố">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Số Điện Thoại</label>
            <input type="text" value="{{$customer->phone}}" name="phone" class="form-control"  placeholder="Số Điện Thoại">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Ghi Chú</label>
            <input type="text" value="{{$customer->note}}" name="note" class="form-control"   placeholder="Ghi Chu">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@stop

